#!/bin/bash

eval $(ssh-agent -s)
ssh-add

for D in Dependencies/*; do
    if [ -d "${D}" ]; then
        if [ -d "${D}/.git" ] || [ -f "${D}/.git" ]; then
            cd "${D}"
            git submodule foreach --recursive "git add --all"
            git submodule foreach --recursive "git commit $@"
            git submodule foreach --recursive "git pull"
            git submodule foreach --recursive "git push"
            git add --all
            git commit "$@"
            git pull
            git push
            cd ../
        fi
    fi
done
for D in *; do
    if [ -d "${D}" ]; then
        if [ -d "${D}/.git" ] || [ -f "${D}/.git" ]; then
            cd "${D}"
            git submodule foreach --recursive "git add --all"
            git submodule foreach --recursive "git commit $@"
            git submodule foreach --recursive "git pull"
            git submodule foreach --recursive "git push"
            git add --all
            git commit "$@"
            git pull
            git push
            cd ../
        fi
    fi
done

echo "Updating main repo"
git pull
git add --all
git commit $@
git push