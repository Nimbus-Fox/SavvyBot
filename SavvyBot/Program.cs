using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using SaplingFramework;
using SaplingFramework.Classes;
using SaplingFramework.Context;

namespace SavvyBot {
    internal class Program {
        internal static ConcurrentDictionary<ulong, int> MrrCount = new ConcurrentDictionary<ulong, int>();
        public static void Main(string[] args) {
            Instance.RegisterEvents += () => {
                GlobalContext.Client.MessageReceived += message => {
                    AddMessage(message);
                    return Task.CompletedTask;
                };
            };
            Startup.Start("SavvyBot", args);
        }

        internal static void AddMessage(IMessage message) {
            if (message.Content.ToLower().Contains("mrr") && !message.Content.ToLower().StartsWith(GlobalContext.Settings.CommandChar)) {
                if (MrrCount.ContainsKey(message.Author.Id)) {
                    var updated = false;
                    while (!updated) {
                        updated = MrrCount.TryUpdate(message.Author.Id, MrrCount[message.Author.Id] + 1,
                            MrrCount[message.Author.Id]);
                    }
                } else {
                    var added = false;
                    while (!added) {
                        added = MrrCount.TryAdd(message.Author.Id, 1);
                    }
                }
                        
                GlobalContext.Logger.WriteLine($"{message.Author.Username} mrrd {MrrCount[message.Author.Id]} times", LogType.Debug);
            }
        }
    }
}