using System;
using System.Linq;
using System.Text;
using Discord;
using Discord.WebSocket;
using SaplingFramework.Context;
using SaplingFramework.Extensions;
using SaplingFramework.Interfaces;

namespace SavvyBot {
    public class MrrCommand : ICommand {
        public string Command => "command.mrr";

        public void Execute(string[] bits, SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime) {
            if (bits.Length >= 2) {
                var sb = new StringBuilder();

                for (var i = 1; i < bits.Length; i++) {
                    if (sb.Length != 0) {
                        sb.Append(" ");
                    }

                    sb.Append(bits[i]);
                }

                ReportUserMrrCount(sb.ToString(), user, channel, dateTime);
            } else {
                ReportSelfMrrCount(user, channel, dateTime);
            }
        }

        public string HandleHelp(string[] bits, SocketUser user) {
            return "";
        }

        public string Description => "command.mrr.description";

        public bool AllowedInDm => true;

        public string PermissionNode => "user.command.mrr";

        private void ReportSelfMrrCount(SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime) {
            SendResult(user.Id, "", user.Username, user.DiscriminatorValue, channel);
        }

        private void SendResult(ulong id, string nickname, string name, ushort discriminator,
            ISocketMessageChannel channel) {
            var embed = new EmbedBuilder();
            if (string.IsNullOrWhiteSpace(nickname)) {
                embed.WithTitle($"{name}#{discriminator}");
            } else {
                embed.WithTitle($"{nickname} ({name}#{discriminator})");
            }

            if (Program.MrrCount.ContainsKey(id)) {
                embed.WithDescription($"{Program.MrrCount[id]} mrrs");
            } else {
                embed.WithDescription("0 mrrs");
            }

            channel.SendMessageAsync(embed: embed.Build());
        }

        private void ReportUserMrrCount(string target, SocketUser user, ISocketMessageChannel channel,
            DateTimeOffset dateTime) {
            var targetLower = target.ToLower();

            foreach (var cUser in channel.GetChannelUsers()) {
                if (cUser.Nickname?.ToLower().StartsWith(targetLower) == true ||
                    cUser.Username.ToLower().StartsWith(targetLower) ||
                    (cUser.Username.ToLower() + "#" + cUser.DiscriminatorValue) == targetLower) {
                    SendResult(cUser.Id, cUser.Nickname, cUser.Username, cUser.DiscriminatorValue, channel);
                    return;
                }
            }

            channel.SendMessageAsync($"Unable to find the snuggler '{target}' D:");
        }
    }
}