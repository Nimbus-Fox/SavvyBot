using System;
using System.Linq;
using System.Threading;
using Discord;
using Discord.WebSocket;
using SaplingFramework.Extensions;
using SaplingFramework.Interfaces;

namespace SavvyBot {
    public class ScanCommand : ICommand {
        public string Command => "command.scan";
        public void Execute(string[] bits, SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime) {
            if (channel.TryGetSocketGuild(out var guild)) {
                if (guild.OwnerId != user.Id) {
                    return;
                }

                var message = channel.GetMessagesAsync(100, CacheMode.AllowDownload,
                    new RequestOptions() {AuditLogReason = "Scan to look for mrrs"});


                while (message.IsEmpty().Result) {
                    
                }

                var nim = message.GetEnumerator();

                nim.MoveNext(CancellationToken.None);
                
                Scan(channel, nim.Current.First());
            }
        }

        private void Scan(ISocketMessageChannel channel, IMessage last) {
            var messages = channel.GetMessagesAsync(last, Direction.Before, 100, CacheMode.AllowDownload,
                new RequestOptions() {AuditLogReason = "Scan to look for mrrs"});

            IMessage superLast = null;

            messages.ForEachAsync((current, i) => {
                foreach (var mes in current) {
                    Program.AddMessage(mes);
                }

                superLast = current.LastOrDefault();
            });

            if (superLast != null) {
                Scan(channel, superLast);
            }
        }

        public string HandleHelp(string[] bits, SocketUser user) {
            return "";
        }

        public string Description => "commands.scan.description";
        public bool AllowedInDm => false;
        public string PermissionNode => "admin.scan";
    }
}